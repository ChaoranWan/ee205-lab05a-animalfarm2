///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author @todo Andrew <@todo chaoran@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @02-02-2001
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {

Palila::Palila( string newLocation, enum Color newColor, enum Gender newGender ) {
   location = newLocation;
   isMigratory = "false";
   gender = newGender;
   species = "Loxioides bailleui";    /// Hardcode this... all cats are the same species (this is a is-a relationship)
   featherColor = newColor;       /// A has-a relationship, so it comes through the constructor
//   isMigratory = "false";       /// An is-a relationship, so it's safe to hardcode.  All cats have the same gestation period.
}




/// Print our Cat and name first... then print whatever information Mammal holds.
void Palila::printInfo() {
   cout << "Palila" << endl;
   cout << "   Where From = [" << location << "]" << endl;
   Bird::printInfo();
}

}

